﻿namespace xboxvotes.Services
{
    public interface IApiKeyService
    {
        string GetKey();
    }
}