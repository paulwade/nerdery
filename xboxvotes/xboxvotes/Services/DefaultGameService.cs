﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using xboxvotes.Models;
using xboxvotes.TheNerderyXboxService;

namespace xboxvotes.Services
{
	public class DefaultGameService : IGameService
	{
		private readonly ApplicationDbContext _applicationDbContext;
		private readonly IApiKeyService _keyService;
		private readonly UserManager<ApplicationUser> _userManager;

		public DefaultGameService(IApiKeyService keyService)
		{
			_keyService = keyService;
			_applicationDbContext = new ApplicationDbContext();
			_userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_applicationDbContext));
		}

		/// <summary>
		///     Get all games
		/// </summary>
		/// <returns>Enumerable of XboxGame object</returns>
		public IEnumerable<XboxGame> GetGames()
		{
			using (var client = new XboxVotingServiceSoapClient())
			{
				return client.GetGames(_keyService.GetKey());
			}
		}

		/// <summary>
		///     add a new game
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		public GameResult AddGame(string title)
		{
			if (GameExists(title))
				return GameResult.DuplicateTitle;

			if (!CanUserAct())
				return GameResult.NoMoreActions;

			using (var client = new XboxVotingServiceSoapClient())
			{
				var result = client.AddGame(title, _keyService.GetKey());
				if (result)
				{
					RecordAction();
					return new GameResult
					{
						Message = "Your game has been added to the list of wanted games and one vote has been added.",
						Success = true
					};
				}
			}
			return GameResult.FailedUnknown;
		}

		/// <summary>
		///     Submit a vote for a game
		/// </summary>
		/// <param name="gameid"></param>
		/// <returns></returns>
		public GameResult Vote(int gameid)
		{
			if (!CanUserAct())
				return GameResult.NoMoreActions;

			using (var client = new XboxVotingServiceSoapClient())
			{
				var result = client.AddVote(gameid, _keyService.GetKey());
				if (result)
				{
					RecordAction();
					return GameResult.VoteSubmitted;
				}

				return GameResult.FailedUnknown;
			}
		}

		/// <summary>
		///     Reset a users actions for the day
		/// </summary>
		public void UserActionReset()
		{
			var user = _userManager.FindById(HttpContext.Current.User.Identity.GetUserId());
			user.LastAction = null;
			_userManager.Update(user);
		}

		/// <summary>
		///     Mark a game in the system as owned
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		public void MarkOwned(int gameId)
		{
			using (var client = new XboxVotingServiceSoapClient())
			{
				client.SetGotIt(gameId, _keyService.GetKey());
			}
		}

		public void RecordAction()
		{
			var user = _userManager.FindById(HttpContext.Current.User.Identity.GetUserId());
			user.LastAction = DateTime.Now;
			_userManager.Update(user);
		}

		public bool CanUserAct()
		{
			var user = _userManager.FindById(HttpContext.Current.User.Identity.GetUserId());

			//if the user has no date set then they must have an action left.
			if (!user.LastAction.HasValue)
				return true;

			return user.LastAction.Value.Date != DateTime.Now.Date;
		}

		/// <summary>
		///     Checks for a title in datastore
		/// </summary>
		/// <param name="title">a title title to check the datastore for</param>
		/// <returns>true if the title already exists in the store otherwise false</returns>
		private bool GameExists(string title)
		{
			var allgames = GetGames();
			if (allgames.Any(m => string.Equals(m.Title, title, StringComparison.OrdinalIgnoreCase))) return true;
			return false;
		}
	}
}