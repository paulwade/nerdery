﻿using System.Collections.Generic;
using xboxvotes.Models;
using xboxvotes.TheNerderyXboxService;

namespace xboxvotes.Services
{
	public interface IGameService
	{
		IEnumerable<XboxGame> GetGames();
		GameResult AddGame(string title);
		GameResult Vote(int id);
		void MarkOwned(int gameId);
		void UserActionReset();
	}
}