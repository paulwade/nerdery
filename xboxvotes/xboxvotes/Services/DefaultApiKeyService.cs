﻿using System;
using System.Configuration;
using xboxvotes.TheNerderyXboxService;

namespace xboxvotes.Services
{
    /// <summary>
    /// Gets app setting NerderyAPIKey from the web.config and validates against The Nerdery Xbox service.
    /// </summary>
    public class DefaultApiKeyService : IApiKeyService
    {
        private static string _ApiKey;

        /// <summary>
        ///     Gets the API key from web.config and validates against service.
        /// </summary>
        /// <returns>A valid API key</returns>
        public string GetKey()
        {
            if (String.IsNullOrEmpty(_ApiKey))
            {
                var tempkey = ConfigurationManager.AppSettings.Get("NerderyAPIKey");
                if (!IsValidKey(tempkey))
                {
                    throw new Exception("API Key is invalid check configuration");
                }
                _ApiKey = tempkey;
            }
            return _ApiKey;
        }

        private bool IsValidKey(string key)
        {
            using (var client = new XboxVotingServiceSoapClient())
            {
                return client.CheckKey(key);
            }
        }
    }
}