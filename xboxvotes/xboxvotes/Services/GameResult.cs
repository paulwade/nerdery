﻿namespace xboxvotes.Services
{
	public class GameResult
	{
		public bool Success { get; set; }
		public string Message { get; set; }

		public static GameResult DuplicateTitle
		{
			get
			{
				return new GameResult()
				{
					Message = "A title of that name already exists",
					Success = false
				};
			}
		}

		public static GameResult NoMoreActions
		{
			get
			{
				return new GameResult()
				{
					Message = "You have already used your maximum actions for today. You are only allowed to vote or add a game once per day.",
					Success = false
				};
			}
		}

		public static GameResult VoteSubmitted
		{
			get
			{
				return new GameResult()
				{
					Message = "Thank you your vote was submitted successfully",
					Success = true
				};
			}
		}
		public static GameResult FailedUnknown
		{
			get
			{
				return new GameResult()
				{
					Message = "Sorry, something went wrong. Please try again later.",
					Success = false
				};
			}
		}
	}
}