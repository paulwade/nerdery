﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using xboxvotes.Services;

namespace xboxvotes.AutofacModules
{
	public class DefaultServices : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<DefaultApiKeyService>().As<IApiKeyService>();
			builder.RegisterType<DefaultGameService>().As<IGameService>();
		}
	}
}