﻿using System.ComponentModel.DataAnnotations;
using xboxvotes.TheNerderyXboxService;

namespace xboxvotes.Models
{
	public class XboxGameModel
	{
		public int Id { get; set; }

		[Required]
		public string Title { get; set; }
		public string Status { get; set; }
		public int Votes { get; set; }
	}
}