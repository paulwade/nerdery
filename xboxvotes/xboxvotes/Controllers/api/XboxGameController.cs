﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using xboxvotes.Models;
using xboxvotes.Services;

namespace xboxvotes.Controllers.api
{
	public class XboxGameController : ApiController
	{
		private readonly IGameService _gameService;

		public XboxGameController(IGameService gameService)
		{
			_gameService = gameService;
		}

		/// <summary>
		///     Will return all games
		/// </summary>
		/// <returns></returns>
		public IEnumerable<XboxGameModel> Get()
		{
			//Lets convert to a POCO model instead of the VS generated version.
			var games = _gameService.GetGames();
			var toXboxGameModels = new List<XboxGameModel>();
			foreach (var game in games)
			{
				toXboxGameModels.Add(new XboxGameModel
				{
					Id = game.Id,
					Status = game.Status,
					Title = game.Title,
					Votes = game.Votes
				});
			}
			return toXboxGameModels;
		}

		/// <summary>
		///     Post a XboxGameModel to add a new game to the service.
		/// </summary>
		/// <returns></returns>
		public HttpResponseMessage Post()
		{
			var game = JsonConvert.DeserializeObject<XboxGameModel>(Request.Content.ReadAsStringAsync().Result);
			var result = _gameService.AddGame(game.Title);
			var returnMessage = new HttpResponseMessage();
			returnMessage.StatusCode = HttpStatusCode.Created;
			if (!result.Success)
			{
				returnMessage.StatusCode = HttpStatusCode.BadRequest;
			}

			returnMessage.Content = new StringContent(result.Message);
			return returnMessage;
		}

		/// <summary>
		///     Update a game with a vote
		/// </summary>
		/// <returns></returns>
		public HttpResponseMessage Put()
		{
			var game = JsonConvert.DeserializeObject<XboxGameModel>(Request.Content.ReadAsStringAsync().Result);
			var result = _gameService.Vote(game.Id);
			var returnMessage = new HttpResponseMessage();
			returnMessage.StatusCode = HttpStatusCode.Accepted;
			if (!result.Success)
			{
				returnMessage.StatusCode = HttpStatusCode.BadRequest;
			}

			returnMessage.Content = new StringContent(result.Message);
			return returnMessage;
		}
	}
}