﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using xboxvotes.Services;

namespace xboxvotes.Controllers
{
	public class HomeController : Controller
	{
		private readonly IGameService _gameService;

		public HomeController(IGameService gameService)
		{
			_gameService = gameService;
		}

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult UserReset()
		{
			_gameService.UserActionReset();
			return RedirectToAction("Index");
		}

		public ActionResult ManageGames()
		{
			return View();
		}
	}
}