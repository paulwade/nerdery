﻿angular.module("xboxApp").controller("xboxHomeCtrl", function($scope, $http) {
    $scope.title = "";
    $scope.ShowMessage = false;
    $scope.Message = "";
    $scope.SaveGame = function() {
        sendNewGame();
    };

    $scope.Vote = function(gameid) {
        sendVote(gameid);
    };

    $scope.Games = "";
    GetGames();

    function sendNewGame() {
        $http({
                url: "/api/XboxGame",
                method: "POST",
                data: { 'title': $scope.title }
            })
            .then(function(response) {
                    toastr.success(response.data, "Success");
                    GetGames();
                },
                function(response) {
                    toastr.error(response.data, "Error");
                }
            );
    }

    function sendVote(gameid) {
        $http({
                url: "/api/XboxGame",
                method: "PUT",
                data: { 'Id': gameid }
            })
            .then(function(response) {
                    toastr.success(response.data, "Success");
                    GetGames();
                },
                function(response) {
                    toastr.error(response.data, "Error");
                }
            );
    }

    function GetGames() {
        $http({
                url: "/api/XboxGame",
                method: "GET",
                headers: {
                    'Content-type': "application/json"
                }
            })
            .then(function(response) {
                    $scope.Games = response.data;
                },
                function(response) {
                    toastr.error("Error", response.data);
                }
            );
    }


});