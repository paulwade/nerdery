﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using xboxvotes.AutofacModules;

namespace xboxvotes.App_Start
{
	public static class AutofacConfig
	{
		public static void BuildContainer()
		{
			var builder = new ContainerBuilder();

			//MVC
			builder.RegisterControllers(typeof (MvcApplication).Assembly).InstancePerRequest();

			//WebApi
			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

			builder.RegisterAssemblyModules(typeof (MvcApplication).Assembly);
			builder.RegisterModule<AutofacWebTypesModule>();
			builder.RegisterModule<DefaultServices>();
			var container = builder.Build();

			//Register for MVC controllers
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
			//Register for webapi controllers
			var config = GlobalConfiguration.Configuration;
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
		}
	}
}