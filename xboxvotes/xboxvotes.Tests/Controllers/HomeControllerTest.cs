﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using xboxvotes;
using xboxvotes.Controllers;
using xboxvotes.Models;
using xboxvotes.Services;
using xboxvotes.TheNerderyXboxService;

namespace xboxvotes.Tests.Controllers
{
	[TestClass]
	public class HomeControllerTest
	{
		[TestMethod]
		public void Index()
		{
			 
			var moqGameService = new Mock<IGameService>();
			moqGameService.Setup(m => m.GetGames()).Returns<List<XboxGame>>(m => m = new List<XboxGame>());

			// Arrange
			HomeController controller = new HomeController(moqGameService.Object);

			// Act
			ViewResult result = controller.Index() as ViewResult;

			// Assert
			Assert.IsNotNull(result);
		}

	}
}
